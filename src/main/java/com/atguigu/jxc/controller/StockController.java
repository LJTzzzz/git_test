package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.PublicKey;
import java.util.Map;

/**
 * 库存管理
 */
@RestController
@RequestMapping
public class StockController {

    @Autowired
    private StockService stockService;

    /**
     * 保存报损单
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @PostMapping("/damageListGoods/save")
    public ServiceVO saveDamageListGoods(DamageList damageList, String damageListGoodsStr){
        return stockService.saveDamageListGoods(damageList,damageListGoodsStr);
    }

    /**
     * 新增报溢单
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    @PostMapping("/overflowListGoods/save")
    public ServiceVO saveOverflowListGoods(OverflowList overflowList, String overflowListGoodsStr){
        return stockService.saveOverflowListGoods(overflowList,overflowListGoodsStr);
    }

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("damageListGoods/list")
    public Map<String,Object> listDamageListGoods(String sTime, String eTime){
        return stockService.listDamageListGoods(sTime,eTime);
    }

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    @PostMapping("/damageListGoods/goodsList")
    public Map<String,Object> listDamageGoodsList(Integer damageListId){
        return stockService.listGoodsList(damageListId);
    }

    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("/overflowListGoods/list")
    public Map<String,Object> listOverflowList(String sTime, String eTime){
        return stockService.listOverflowList(sTime,eTime);
    }

    /**
     * 报溢单商品信息
     * @param overflowListId
     * @return
     */
    @PostMapping("/overflowListGoods/goodsList")
    public Map<String,Object> listOverflowListGoods(Integer overflowListId){
        return stockService.listOverflowListGoods(overflowListId);
    }
}
