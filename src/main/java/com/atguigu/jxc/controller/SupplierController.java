package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @description 供应商Controller控制器
 */

@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    @PostMapping("/list")
    public Map<String,Object> list(Integer page, Integer rows, String supplierName){
        Map<String,Object> map = supplierService.list(page,rows,supplierName);
        return map;
    }

    @PostMapping("/save")
    public ServiceVO save(Supplier supplier ){
        return supplierService.save(supplier);
    }

    @PostMapping("/delete")
    public ServiceVO delete(String  ids){
        return supplierService.delete(ids);
    }
}

