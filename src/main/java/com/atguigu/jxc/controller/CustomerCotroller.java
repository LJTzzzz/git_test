package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


/**
 * @description 客户管理Controller层
 */
@RestController
@RequestMapping("/customer")
public class CustomerCotroller {

    @Autowired
    private CustomerService customerService;

    /**
     * 客户列表分页
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> list(Integer page, Integer rows, String  customerName){
        return customerService.list(page,rows,customerName);
    }

    /**
     * 客户添加或修改
     * @param customer
     * @return
     */
    @PostMapping("/save")
    public ServiceVO save(Customer customer){
        return customerService.save(customer);
    }

    /**
     * 客户删除（支持批量删除）
     * @param ids
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO delete(String ids){
       return customerService.delete(ids);
    }
}
