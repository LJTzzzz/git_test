package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Unit;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();


    List<Goods> listInventory(int offSet, Integer rows, String codeOrName, Integer goodsTypeId);


    List<GoodsType> loadGoodsType();


    List<Unit> listUnit();

    List<Goods> listGoods(int offSet, Integer rows, String goodsName, Integer goodsTypeId);

    GoodsType getByGoodsTypeName(String goodsTypeName);

    void saveGoodsType(String goodsTypeName, Integer pId);


    List<Goods> getGoodsByGoodsTypeId(Integer goodsTypeId);

    void deleteGoodsType(Integer goodsTypeId);

    void updateGoods(Goods goods);

    void addGoods(Goods goods);

    Goods getGoodsByGoodsId(Integer goodsId);

    void delete(Integer goodsId);


    List<Goods> getNoInventoryQuantity(int offSet, Integer rows, String nameOrCode);

    List<Goods> getHasInventoryQuantity(int offSet, Integer rows, String nameOrCode);

    void saveStock(Goods goods);

    void deleteStock(Integer goodsId);

    List<Goods> list();
}
