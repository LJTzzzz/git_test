package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;

import java.util.List;

public interface StockDao {

    void saveDamageList(DamageList damageList);

    void saveDamageListGoods(DamageListGoods damageListGoods);

    void saveOverflowList(OverflowList overflowList);

    void saveOverflowListGoods(OverflowListGoods overflowListGoods);

    List<DamageList> listDamageList(String sTime, String eTime);

    List<DamageListGoods> listDamageGoodsList(Integer damageListId);

    List<OverflowList> listOverflowList(String sTime, String eTime);

    List<OverflowListGoods> listOverflowListGoods(Integer overflowListId);

    Integer getOverflowListIdByNumber(String overflowNumber);

    Integer getDamageListIdByNumber(String damageNumber);
}
