package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;

import java.util.List;

public interface CustomerDao {
    List<Customer> list(int offSet, Integer rows, String customerName);

    void add(Customer customer);

    void update(Customer customer);

    void delete(String deleteId);
}
