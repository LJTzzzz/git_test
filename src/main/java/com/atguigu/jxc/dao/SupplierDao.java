package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;

import java.util.List;

public interface SupplierDao {
     List<Supplier> getSupplierList(int offSet, Integer rows, String supplierName);

     Supplier getSupplier(Integer supplierId);

     void update(Supplier supplier);

     void add(Supplier supplier);

    void delete(String id);
}
