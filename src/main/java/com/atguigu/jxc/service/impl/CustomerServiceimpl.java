package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceimpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    /**
     * 客户列表分页
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @Override
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Customer> customers = customerDao.list(offSet,rows,customerName);
        int total = customers.size();
        map.put("total",total);
        map.put("rows",customers);
        return map;
    }

    /**
     * 客户添加或修改
     * @param customer
     * @return
     */
    @Override
    public ServiceVO save(Customer customer) {
        Integer customerId = customer.getCustomerId();
        if (customerId == null){
            //新增
            customerDao.add(customer);
        }else {
            //修改
            customerDao.update(customer);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 客户删除（支持批量删除）
     * @param ids
     * @return
     */
    @Override
    public ServiceVO delete(String ids) {
        String[] deleteIds = ids.split(",");
        for (String deleteId : deleteIds) {
            customerDao.delete(deleteId);
        }
//        customerDao.delete(ids);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }
}
