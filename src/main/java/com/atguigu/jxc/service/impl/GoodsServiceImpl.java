package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    /**
     * 分页查询商品库存信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @Override
    public Map<String,Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.listInventory(offSet, rows, codeOrName,goodsTypeId);
        int total = goodsList.size();
        map.put("total", total);
        map.put("rows", goodsList);

        return map;
    }

    @Override
    public String loadGoodsType() {
        List<GoodsType> goodsTypeList = goodsDao.loadGoodsType();
        List<GoodTypeTree> typeTrees = new ArrayList<>();
        List<GoodTypeTree> goodsTypeTreeList = new ArrayList<>();
        for (GoodsType goodsType : goodsTypeList) {
            GoodTypeTree goodTypeTree = translation(goodsType);
            goodsTypeTreeList.add(goodTypeTree);
        }
        for (GoodTypeTree goodTypeTree : goodsTypeTreeList) {
            if (goodTypeTree.getPId() == -1){
                typeTrees.add(findChildren(goodTypeTree,goodsTypeTreeList));
            }
        }
        return JSONObject.toJSON(typeTrees).toString();
    }

    //转化
    public GoodTypeTree translation(GoodsType goodsType){

        GoodTypeTree goodTypeTree = new GoodTypeTree();
        goodTypeTree.setId(goodsType.getGoodsTypeId());
        goodTypeTree.setText(goodsType.getGoodsTypeName());
        Integer typeState = goodsType.getGoodsTypeState();
        if (typeState != null){
            goodTypeTree.setState((typeState == 0)?"open":"closed");
        }
        goodTypeTree.setIconCls("goods-type");
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("state",typeState);
        goodTypeTree.setAttributes(hashMap);
        goodTypeTree.setPId(goodsType.getPId());
        return goodTypeTree;
    }

    private GoodTypeTree findChildren(GoodTypeTree goodTypeTree, List<GoodTypeTree> goodTypeTreeList) {
        goodTypeTree.setChildren(new ArrayList<>());
        for (GoodTypeTree goodType : goodTypeTreeList) {
            if (goodTypeTree.getId().equals(goodType.getPId())){
                if (goodTypeTree.getChildren() == null){
                    goodTypeTree.setChildren(new ArrayList<>());
                }
                goodTypeTree.getChildren().add(findChildren(goodType,goodTypeTreeList));
            }
        }
        return goodTypeTree;
    }



    @Override
    public Map<String, Object> listUnit() {
        Map<String, Object> map = new HashMap<>();
        List<Unit> unitList = goodsDao.listUnit();
        map.put("rows",unitList);
        return map;
    }

    /**
     * 分页查询商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @Override
    public Map<String, Object> listGoods(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0 ? 1:page;
        int offSet = (page-1) * rows;
        List<Goods> goodsList = goodsDao.listGoods(offSet,rows,goodsName,goodsTypeId);
        int total = goodsList.size();
        map.put("total",total);
        map.put("rows",goodsList);
        return map;
    }

    /**
     * 新增分类
     * @return
     */
    @Override
    public ServiceVO saveGoodsType(String goodsTypeName, Integer pId) {
        GoodsType goodsType = goodsDao.getByGoodsTypeName(goodsTypeName);
        if (goodsType != null){
            return new ServiceVO(ErrorCode.GOODSTYPE_EXIST_CODE,ErrorCode.GOODSTYPE_EXIST_MESS);
        }
        goodsDao.saveGoodsType(goodsTypeName,pId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除分类
     * @param goodsTypeId
     * @return
     */
    @Override
    public ServiceVO deleteGoodsType(Integer goodsTypeId) {
        List<Goods> goodsList = goodsDao.getGoodsByGoodsTypeId(goodsTypeId);
        if (!CollectionUtils.isEmpty(goodsList)){
            //分类下有商品
            return new ServiceVO(ErrorCode.GOODS_TYPE_ERROR_CODE,ErrorCode.GOODS_TYPE_ERROR_MESS);
        }
        goodsDao.deleteGoodsType(goodsTypeId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     *
     * @param goods
     * @return
     */
    @Override
    public ServiceVO saveGoods(Goods goods) {
        if (goods.getGoodsId() != null){
            //修改
            goodsDao.updateGoods(goods);
        }else {
            //库存不能为空
            if (goods.getInventoryQuantity() == null){
                goods.setInventoryQuantity(0);
            }
            System.out.println("新增商品");
            //状态不能为空
            if (goods.getState() == null){
                goods.setState(0);
            }
            goodsDao.addGoods(goods);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除商品信息
     * @param goodsId 商品ID
     * @return
     */
    @Override
    public ServiceVO deleteGoods(Integer goodsId) {
        Goods goods = goodsDao.getGoodsByGoodsId(goodsId);
        Integer state = goods.getState();
        if (state == 2 ){
            return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
        }else if (state == 1){
            return new ServiceVO(ErrorCode.STORED_ERROR_CODE,ErrorCode.STORED_ERROR_MESS);
        }
        goodsDao.delete(goodsId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 分页查询无库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        page = page == 0 ? 1:page;
        int offSet = (page -1 ) * rows;
        List<Goods> noInventoryGoodsList =  goodsDao.getNoInventoryQuantity(offSet,rows,nameOrCode);
        int total = noInventoryGoodsList.size();
        Map<String, Object> map = new HashMap<>();
        map.put("total",total);
        map.put("rows",noInventoryGoodsList);
        return map;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        page = page == 0 ? 1:page;
        int offSet = (page -1 ) * rows;
        List<Goods> hasInventoryQuantity =  goodsDao.getHasInventoryQuantity(offSet,rows,nameOrCode);
        int total = hasInventoryQuantity.size();
        Map<String, Object> map = new HashMap<>();
        map.put("total",total);
        map.put("rows",hasInventoryQuantity);
        return map;
    }

    /**
     * 添加商品期初库存
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */
    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        Goods goods = new Goods();
        goods.setGoodsId(goodsId);
        goods.setInventoryQuantity(inventoryQuantity);
        goods.setPurchasingPrice(purchasingPrice);
        goodsDao.saveStock(goods);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除商品库存
     * @param goodsId 商品ID
     * @return
     */
    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        Goods goods = goodsDao.getGoodsByGoodsId(goodsId);
        Integer state = goods.getState();
        if (state == 2 ){
            return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
        }else if (state == 1){
            return new ServiceVO(ErrorCode.STORED_ERROR_CODE,ErrorCode.STORED_ERROR_MESS);
        }
        goodsDao.deleteStock(goodsId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 查询库存报警商品信息
     * @return
     */
    @Override
    public Map<String, Object> listAlarm() {
        Map<String, Object> map = new HashMap<>();
        List<Goods> goodsList = goodsDao.list();
        map.put("rows",goodsList);
        return map;
    }




}
