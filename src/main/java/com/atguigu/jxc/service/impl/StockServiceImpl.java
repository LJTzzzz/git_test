package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.jxc.dao.StockDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StockServiceImpl implements StockService {

    @Autowired
    private StockDao stockDao;

    /**
     * 保存报损单
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @Override
    public ServiceVO saveDamageListGoods(DamageList damageList, String damageListGoodsStr) {

        stockDao.saveDamageList(damageList);
        String damageNumber = damageList.getDamageNumber();
        Integer damageListId = stockDao.getDamageListIdByNumber(damageNumber);

        //关联
        List<DamageListGoods> damageListGoodsList = JSONObject.parseArray(damageListGoodsStr, DamageListGoods.class);
        for (DamageListGoods damageListGoods : damageListGoodsList) {
            damageListGoods.setDamageListId(damageListId);
            stockDao.saveDamageListGoods(damageListGoods);
        }

        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 新增报溢单
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    @Override
    public ServiceVO saveOverflowListGoods(OverflowList overflowList, String overflowListGoodsStr) {

        stockDao.saveOverflowList(overflowList);
        String overflowNumber = overflowList.getOverflowNumber();
        Integer overflowListId = stockDao.getOverflowListIdByNumber(overflowNumber);
        //关联

        List<OverflowListGoods> overflowListGoodsList = JSONObject.parseArray(overflowListGoodsStr, OverflowListGoods.class);
        for (OverflowListGoods overflowListGoods : overflowListGoodsList) {
            overflowListGoods.setOverflowListId(overflowListId);
            stockDao.saveOverflowListGoods(overflowListGoods);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> listDamageListGoods(String sTime, String eTime) {
        Map<String, Object> map = new HashMap<>();
        List<DamageList> damageListList = stockDao.listDamageList(sTime,eTime);
        map.put("rows",damageListList);
        return map;
    }

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    @Override
    public Map<String, Object> listGoodsList(Integer damageListId) {
        Map<String, Object> map = new HashMap<>();
        List<DamageListGoods> damageListGoodsList = stockDao.listDamageGoodsList(damageListId);
        map.put("rows",damageListGoodsList);
        return map;
    }

    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> listOverflowList(String sTime, String eTime) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowList> overflowLists = stockDao.listOverflowList(sTime,eTime);
        map.put("rows",overflowLists);
        return map;
    }

    /**
     * 报溢单商品信息
     * @param overflowListId
     * @return
     */
    @Override
    public Map<String, Object> listOverflowListGoods(Integer overflowListId) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowListGoods> overflowListGoodsList = stockDao.listOverflowListGoods(overflowListId);
        map.put("rows",overflowListGoodsList);
        return map;
    }
}
