package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;

import java.util.Map;

public interface StockService {
    ServiceVO saveDamageListGoods(DamageList damageList, String damageListGoodsStr);

    ServiceVO saveOverflowListGoods(OverflowList overflowList, String overflowListGoodsStr);

    Map<String, Object> listDamageListGoods(String sTime, String eTime);

    Map<String, Object> listGoodsList(Integer damageListId);

    Map<String, Object> listOverflowList(String sTime, String eTime);

    Map<String, Object> listOverflowListGoods(Integer overflowListId);

}
